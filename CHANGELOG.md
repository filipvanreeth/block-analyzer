# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Added `assets/dist` folder for distribution ([#1](https://gitlab.com/filipvanreeth/block-analyzer/-/merge_requests/1))
- Styling blocks on pages and posts ([#2](https://gitlab.com/filipvanreeth/block-analyzer/-/merge_requests/2))
