# Block Analyzer Plugin

The Block Analyzer plugin is a tool for analyzing the blocks used in your WordPress site. It provides information about the blocks used on each page, including their names, attributes, and other details.

## Installation

To install the Block Analyzer plugin, follow these steps:

1. Download the plugin files from the [WordPress plugin repository](https://wordpress.org/plugins/block-analyzer/) or from the [Gitlab repository](https://gitlab.com/filipvanreeth/block-analyzer).
2. Upload the plugin files to the `/wp-content/plugins/block-analyzer` directory, or install the plugin through the WordPress plugins screen directly.
3. Activate the plugin through the 'Plugins' screen in WordPress.

## Usage

To use the Block Analyzer plugin, simply activate it and navigate to any page on your WordPress site. The plugin will automatically analyze the blocks used on the page and display the block used in (custom) posts and pages.

## Configuration

The Block Analyzer plugin can be configured using the following filters:

-   `block_analyzer/disable_block`: Disables a specific block(s) from being analyzed.
-   `block_analyzer/disable_core_blocks`: Disables all core blocks from being analyzed.

### Examples

```php
// Disables the core/paragraph and core/columns blocks from being analyzed.

add_filter('block_analyzer/disable_block', function ($blocks) {
  $blocks[] = 'core/paragraph';
  $blocks[] = 'core/columns';
  return $blocks;
});

// Disables all core blocks from being analyzed.

add_filter('block_analyzer/disable_core_blocks', function ($status): bool {
	return false;
});

```
## Contributing

Contributions to the Block Analyzer plugin are welcome! To contribute, please follow these steps:

1. Fork the [repository](https://gitlab.com/filipvanreeth/block-analyzer).
2. Create a new branch for your changes.
3. Make your changes and commit them.
4. Push your changes to your fork.
5. Submit a pull request.

## License

The Block Analyzer plugin is licensed under the GPL-2.0+ license.
