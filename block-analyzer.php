<?php
/**
 * Plugin Name:     Block Analyzer
 * Description:     Block Analyzer plugin that helps you to analyze & find the (Gutenberg) blocks used on your site. Easily find out which blocks are used on which posts/pages.
 * Author:          Filip Van Reeth
 * Author URI:      filipvanreeth.com
 * Text Domain:     block-analyzer
 * Domain Path:     /languages
 * Version:         1.0.1
 * Network:         true
 * Requires PHP:    8.0
 *
 * @package         Block_Analyzer
 */

use Filip_Van_Reeth\Block_Analyzer\Block_Analyzer;

// Bail if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

// Load Composer autoloader.
if ( ! class_exists( 'Filip_Van_Reeth\\Block_Analyzer\\Block_Analyzer' ) ) {
	$block_analyzer_dir = __DIR__;

	if ( ! file_exists( "{$block_analyzer_dir}/vendor/autoload.php" ) ) {
		wp_die( __( 'Please run composer install in the Block Analyzer plugin directory', 'block-analyzer' ) );
	}

	// Load plugin Composer autoloader.
	require_once "{$block_analyzer_dir}/vendor/autoload.php";

	unset( $block_analyzer_dir );
}

/**
 * Initializes the Block Analyzer plugin.
 * @return Block_Analyzer
 */
function block_analyzer(): Block_Analyzer {
	return new Block_Analyzer();
}

block_analyzer();
