const path = require('path');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
	entry: {
		blockAnalyzer: './assets/src/js/index.js',
	},
	output: {
		filename: 'js/index.js',
		path: path.resolve(__dirname, 'assets/dist'),
		publicPath: 'assets/dist/',
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							importLoaders: 1,
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
						},
					},
				],
			},
		],
	},
	plugins: [
		new CleanWebpackPlugin({
			cleanStaleWebpackAssets: true,
			protectWebpackAssets: false,
		}),
		new MiniCssExtractPlugin({
			filename: 'css/style-[contenthash].css',
		}),
		new WebpackManifestPlugin({
			fileName: path.resolve(__dirname, 'manifest.json'),
		}),
	],
};
