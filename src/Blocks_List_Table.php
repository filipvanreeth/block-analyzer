<?php
declare(strict_types=1);

namespace Filip_Van_Reeth\Block_Analyzer;

class Blocks_List_Table extends \WP_List_Table {

	public function __construct() {
		parent::__construct(
			array(
				'singular' => 'block',
				'plural' => 'blocks',
				'ajax' => false,
			)
		);
	}

	/**
	 * Gets a list of columns.
	 * @return array<string, string>
	 */
	public function get_columns(): array {
		return array(
			'name' => __( 'Name', 'block-analyzer' ),
			'count' => __( 'Count', 'block-analyzer' ),
			'posts' => __( 'Posts', 'block-analyzer' ),
		);
	}

	private function get_blocks(): array {
		$transient = get_transient( 'block_analyzer_list_table_blocks' );

		if ( $transient ) {
			return $transient;
		}

		$blocks = array();

		$block_types = \WP_Block_Type_Registry::get_instance()->get_all_registered();

		$classic_block_type = array(
			'classic-block' => (object) array(
				'name' => Block_Usage::get_classic_block_name(),
			),
		);

		$block_types = array_merge( $block_types, $classic_block_type );

		usort(
			$block_types,
			function ($a, $b) {
				return $a->name <=> $b->name;
			}
		);

		foreach ( $block_types as $block_type ) {

			$posts = $this->get_posts_by_block( $block_type->name )['posts'];

			$render_posts_html = array();

			foreach ( $posts as $post ) {
				$render_posts_html[] = sprintf(
					'<a href="%s" style="white-space: nowrap">%s</a>',
					get_edit_post_link( $post->ID ),
					$post->post_title
				);
			}

			$render_posts_html = implode( ', ', $render_posts_html );

			$blocks[] = array(
				'name' => $block_type->name,
				'count' => $this->get_posts_by_block( $block_type->name )['count'],
				'posts' => $render_posts_html,
			);
		}

		set_transient( 'block_analyzer_list_table_blocks', 0 );

		return $blocks;
	}

	private function get_posts_by_block( string $block ): array {
		$registered_post_types = get_post_types( array( 'public' => true ), 'names' );

		$args = array(
			'post_type' => $registered_post_types,
			'posts_per_page' => -1,
		);

		$posts = get_posts( $args );

		$postsArray = array();

		$count = 0;

		$posts_arr = array();

		foreach ( $posts as $post ) {
			$block_usage = new Block_Usage( post_id: $post->ID );
			$block_usage->initialize_blocks();
			$block_posts = $block_usage->get_blocks();

			if ( ! $block_posts ) {
				$postsArray['count'] = 0;
				$postsArray['posts'] = '';
			}

			if ( in_array( $block, $block_posts ) ) {
				$count_block = array_count_values( $block_posts );
				$count += $count_block[ $block ];
				$posts_arr[] = $post;
			}
		}

		$postsArray['count'] = $count;
		$postsArray['posts'] = $posts_arr;

		return $postsArray;
	}

	public function prepare_items(): void {
		$blocks = $this->get_blocks();

		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();

		$this->_column_headers = array( $columns, $hidden, $sortable );

		$per_page = $this->get_items_per_page( 'blocks_per_page', 20 );
		$current_page = $this->get_pagenum();
		$total_items = count( $blocks );

		usort( $blocks, function ($a, $b) {
			$orderby = $_GET['orderby'] ?? 'name';
			$order = $_GET['order'] ?? 'asc';

			$result = $a[ $orderby ] <=> $b[ $orderby ];

			return ( 'asc' === $order ) ? $result : -$result;
		} );

		$page_items = array_slice( $blocks, ( ( $current_page - 1 ) * $per_page ), $per_page );

		$this->set_pagination_args(
			array(
				'total_items' => $total_items,
				'per_page' => $per_page,
				'total_pages' => (int) ceil( $total_items / $per_page ),
			)
		);

		$this->items = $page_items;

		$search_key = isset( $_GET['s'] ) ? wp_unslash( trim( $_GET['s'] ) ) : '';

		if ( $search_key ) {
			$this->items = $this->filter_table_data( $this->items, $search_key );
		}
	}

	public function display_tablenav( $which ) {
		if ( 'top' === $which ) {
			wp_nonce_field( 'bulk-' . $this->_args['plural'] );
		}
		?>
		<div class="tablenav <?php echo esc_attr( $which ); ?>">
			<div class="alignleft actions bulkactions">
				<?php $this->bulk_actions( $which ); ?>
			</div>
			<?php
			$this->extra_tablenav( $which );
			$this->pagination( $which );
			?>
			<br class="clear" />
		</div>
		<?php
	}

	public function extra_tablenav( $which ) {
		if ( $which == "top" ) {
			$current_screen = get_current_screen();
			$parent_file = $current_screen->parent_file;
			$admin_url = admin_url( $parent_file );
			?>
			<form method="GET" action="<?php echo esc_url( $admin_url ) ?>">
				<div class="alignleft actions">
					<input type="hidden" name="page" value="block-analyzer" />
					<input type="text" name="s" value="<?php echo isset( $_GET['s'] ) ? $_GET['s'] : ''; ?>" />
					<input type="submit" class="button" value="Search" />
				</div>
			</form>
			<?php
		}
	}

	public function filter_table_data( $table_data, $search_key ) {
		$filtered_table_data = array_values( array_filter( $table_data, function ($row) use ($search_key) {

			foreach ( $row as $row_val ) {

				if ( stripos( (string) $row_val, $search_key ) !== false ) {
					return true;
				}
			}

			return false;
		} ) );

		return $filtered_table_data;
	}

	public function column_default( $item, $column_name ): mixed {
		switch ( $column_name ) {
			case 'name':
				return $item['name'];
			case 'count':
				return $item['count'];
			case 'posts':
				return $item['posts'];
			default:
				return null;
		}
	}

	public function get_sortable_columns(): array {
		$sortable_columns = array(
			'name' => array( 'name', true ),
			'count' => array( 'count', true ),
		);

		return $sortable_columns;
	}
}
