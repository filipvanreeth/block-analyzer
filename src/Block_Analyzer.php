<?php
/**
 * Main Block Analyzer class
 *
 * @package block-analyzer
 */

namespace Filip_Van_Reeth\Block_Analyzer;

use WP_Admin_Bar;

/**
 * Main Block Analyzer class
 */
class Block_Analyzer {

	private string $version = '1.0.1';

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->define_constants();

		// Register hooks.
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		// Actions.
		add_action( 'plugins_loaded', array( $this, 'action_plugins_loaded' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		add_action( 'manage_posts_custom_column', array( $this, 'manage_posts_custom_column' ), 10, 2 );
		add_action( 'manage_pages_custom_column', array( $this, 'manage_pages_custom_column' ), 10, 2 );
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'admin_bar_menu', array( $this, 'add_button_to_admin_bar' ), 100, 1 );
		add_action( 'save_post', array( $this, 'delete_block_analyzer_transient_on_save_post' ) );

		// Filters.
		add_filter( 'manage_posts_columns', array( $this, 'add_posts_columns' ), 10, 2 );
		add_filter( 'manage_pages_columns', array( $this, 'add_pages_columns' ), 10, 1 );
		add_filter( 'set-screen-option', array( $this, 'add_admin_menu' ), 100, 3 );
	}

	/**
	 * Defines the constants
	 * @return void
	 */
	public function define_constants(): void {
		define( 'BLOCK_ANALYZER_VERSION', $this->version );
		define( 'BLOCK_ANALYZER_DIR_PATH', trailingslashit( dirname( plugin_dir_path( __FILE__ ) ) ) );
		define( 'BLOCK_ANALYZER_DIR_URL', trailingslashit( dirname( plugin_dir_url( __FILE__ ) ) ) );
		define( 'BLOCK_ANALYZER_BASENAME', dirname( plugin_basename( __FILE__ ) ) );
		define( 'BLOCK_ANALYZER_FILE', __FILE__ );
		define( 'BLOCK_ANALYZER_DEBUG', false );
		define( 'BLOCK_ANALYZER_TEXT_DOMAIN', 'block-analyzer' );
		define( 'BLOCK_ANALYZER_MINIMUM_WP_VERSION', '6.0' );
		define( 'BLOCK_ANALYZER_MINIMUM_PHP_VERSION', '8.0' );
	}

	/**
	 * Runs on plugin activation
	 * @return void
	 */
	public function activate(): void {
		// Do nothing.
	}

	/**
	 * Runs on plugin deactivation
	 * @return void
	 */
	public function deactivate(): void {
		// Do nothing.
	}

	/**
	 * plugins_loaded action hooks
	 */
	public function action_plugins_loaded(): void {
		load_plugin_textdomain(
			BLOCK_ANALYZER_TEXT_DOMAIN,
			false,
			dirname( plugin_basename( __FILE__ ) ) . '/languages'
		);
	}

	/**
	 * Gets the manifest file
	 */
	private function get_manifest(): array {
		$manifest_path = BLOCK_ANALYZER_DIR_PATH . 'manifest.json';

		if ( ! file_exists( $manifest_path ) ) {
			return array();
		}

		$manifest = json_decode( file_get_contents( $manifest_path ), true );

		return $manifest;
	}

	/**
	 * admin_enqueue_scripts action hook
	 */
	public function admin_enqueue_scripts(): void {
		$manifest = $this->get_manifest();

		if ( ! isset( $manifest['blockAnalyzer.css'] ) ) {
			return;
		}

		$file_path = $manifest['blockAnalyzer.css'];

		wp_enqueue_style(
			'block-analyzer',
			BLOCK_ANALYZER_DIR_URL . $file_path,
			array(),
			filemtime( BLOCK_ANALYZER_DIR_PATH . $file_path )
		);
	}

	/**
	 * Renders the column
	 */
	private function render_column( string $column, int $post_id ): void {
		if ( 'block_analyzer_blocks_usage' === $column ) {
			$block_usage = new Block_Usage( post_id: $post_id );
			$block_usage->initialize_blocks();
			$block_usage->render_blocks();
		}
	}

	/**
	 * Adds columns to the posts overview
	 */
	public function add_posts_columns( array $columns, string $post_type ): array {
		$post_types = array( 'post' );
		$post_types = apply_filters( 'block_analyzer/add_post_types', $post_types );

		if ( ! $post_types ) {
			return $columns;
		}

		if ( ! in_array( $post_type, $post_types ) ) {
			return $columns;
		}

		$columns['block_analyzer_blocks_usage'] = __( 'Block Usage', 'block-analyzer' );

		return $columns;
	}

	/**
	 * Renders the post column
	 */
	public function manage_posts_custom_column( string $column, int $post_id ): void {
		$this->render_column( column: $column, post_id: $post_id );
	}

	/**
	 * Adds columns to the pages overview
	 */
	public function add_pages_columns( array $columns ): array {
		$columns['block_analyzer_blocks_usage'] = __( 'Blocks', 'block-analyzer' );

		return $columns;
	}

	/**
	 * Renders the page column
	 */
	public function manage_pages_custom_column( string $column, int $post_id ): void {
		$this->render_column( column: $column, post_id: $post_id );
	}

	/**
	 * Adds a button to the admin bar.
	 *
	 * @param WP_Admin_Bar $wp_admin_bar The WordPress admin bar object.
	 * @return void
	 */
	public function add_button_to_admin_bar( WP_Admin_Bar $wp_admin_bar ): void {
		$args = array(
			'id' => 'block-analyzer',
			'parent' => null,
			// 'group' => 'block-analyzer',
			'title' => sprintf(
				'<span class="ab-icon dashicons dashicons-superhero"></span>%s',
				__( 'Block Analyzer', 'block-analyzer' )
			),
			'href' => admin_url( 'themes.php?page=block-analyzer' ),
			'meta' => array(
				'class' => 'block-analyzer-button',
			),
		);

		$wp_admin_bar->add_node( $args );
	}

	/**
	 * Adds the Blocks Analyzer admin menu
	 */
	public function add_admin_menu(): void {
		add_submenu_page(
			'themes.php',
			__( 'Block Analyzer', 'block-analyzer' ),
			__( 'Block Analyzer', 'block-analyzer' ),
			'manage_options',
			'block-analyzer',
			array( $this, 'admin_menu_callback' )
		);

		add_action( 'load-$hook', array( $this, 'screen_options' ) );
	}

	/**
	 * Sets the screen options
	 */
	public function screen_options(): void {
		$option = 'per_page';
		$args = array(
			'label' => __( 'Blocks', 'block-analyzer' ),
			'default' => 10,
			'option' => 'blocks_per_page',
		);

		add_screen_option( $option, $args );
	}

	/**
	 * Deletes the block analyzer transient on save post.
	 *
	 * This function is responsible for deleting the transient 'block_analyzer_list_table_blocks'
	 * when a post is saved. It checks if the post is a revision and returns early if true.
	 *
	 * @param integer $post_id The ID of the post being saved.
	 * @return void
	 */
	public function delete_block_analyzer_transient_on_save_post( int $post_id ): void {
		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		delete_transient( 'block_analyzer_list_table_blocks' );
	}

	/**
	 * Renders the Block Analyzer admin options page
	 */
	public function admin_menu_callback(): void {
		$blocks_list_table = new Blocks_List_Table();
		$blocks_list_table->prepare_items();
		?>
		<div class="wrap">
			<h2>
				<?php echo esc_html__( 'Blocks', 'block-analyzer' ); ?>
			</h2>
			<p>
				<?php
				echo esc_html__(
					'This table shows which blocks are used on pages, posts and custom post types.',
					'block-analyzer'
				);
				?>
			</p>
			<?php
			// @codingStandardsIgnoreStart
			// Ignoring this because we've verified that $blocks_list_table->display() is safe and does not need escaping.
			echo $blocks_list_table->display();
			// @codingStandardsIgnoreEnd
			?>
		</div>
		<?php
	}
}
