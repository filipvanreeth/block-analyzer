<?php
/**
 * Block Usage class
 *
 * @package block-analyzer
 */

namespace Filip_Van_Reeth\Block_Analyzer;

/**
 * Block Usage class
 */
class Block_Usage {

	private int $post_id;
	private array $blocks;

	public static string $classic_block_name = 'classic-block';

	public function __construct( int $post_id ) {
		$this->post_id = $post_id;
	}

	/**
	 * Initializes the blocks.
	 * @return void
	 */
	public function initialize_blocks() {
		$parsed_blocks = parse_blocks( get_post_field( 'post_content', $this->post_id ) );
		$blocks = $this->extract_block_names( $parsed_blocks );

		$this->blocks = $blocks;
	}

	/**
	 * Returns all registered blocks.
	 * @return array
	 */
	private function get_registered_blocks(): array {
		$blocks = array();

		$block_types = \WP_Block_Type_Registry::get_instance()->get_all_registered();

		foreach ( $block_types as $block_type ) {
			$blocks[] = $block_type->name;
		}

		return $blocks;
	}

	/**
	 * Returns all core blocks.
	 * @return array
	 */
	private function get_all_core_blocks(): array {
		$all_registered_blocks = $this->get_registered_blocks();

		$core_blocks = array_filter(
			$all_registered_blocks,
			function ( $block ) {
				preg_match( '/^core(-embed)?\/[a-z-]*$/', $block, $matches );

				if ( $matches ) {
					return $block;
				}
			}
		);

		return $core_blocks;
	}

	/**
	 * Disables core blocks.
	 * @param mixed $blocks
	 * @return mixed
	 */
	private function disable_core_blocks( $blocks ) {
		$core_blocks = $this->get_all_core_blocks();
		$blocks = array_diff( $blocks, $core_blocks );

		return $blocks;
	}

	/**
	 * Disables block types.
	 * @param mixed $blocks
	 * @return mixed
	 */
	private function disable_block_types( $blocks ) {
		$disable_blocks = apply_filters(
			'block_analyzer/disable_block',
			array(),
			get_post_type( $this->post_id )
		);

		$blocks = array_diff( $blocks, $disable_blocks );

		return $blocks;
	}

	/**
	 * Extracts the block names.
	 * @param mixed $blocks
	 * @param mixed $result
	 * @return mixed
	 */
	private function extract_block_names( $blocks, &$result = array() ) {
		foreach ( $blocks as $block ) {
			if(!$block['blockName']) {
				$result[] = self::get_classic_block_name();
			}

			if ( isset( $block['blockName'] ) ) {
				$result[] = $block['blockName'];
			}

			if ( isset( $block['innerBlocks'] ) && ! empty( $block['innerBlocks'] ) ) {
				$this->extract_block_names( $block['innerBlocks'], $result );
			}
		}

		return $result;
	}

	/**
	 * Returns the class block name.
	 */
	public static function get_classic_block_name(): string {
		return self::$classic_block_name;
	}

	/**
	 * Returns the blocks.
	 * @return array
	 */
	public function get_blocks(): array {
		return $this->blocks;
	}

	/**
	 * Renders the blocks.
	 * @return void
	 */
	public function render_blocks() {
		$blocks = $this->disable_block_types( $this->blocks );

		$disable_core_blocks = apply_filters( 'block_analyzer/disable_core_blocks', false );

		if ( $disable_core_blocks ) {
			$blocks = $this->disable_core_blocks( $blocks );
		}

		$blocks = array_count_values( $blocks );

		echo $this->generate_blocks_html( $blocks );
	}

	/**
	 * Generates the blocks html.
	 * @param mixed $blocks
	 * @return string
	 */
	public function generate_blocks_html( $blocks ): string {
		if ( ! $blocks ) {
			$render = '<div class="block-analyzer-blocks">';
			$render .= '<div class="block block--no-blocks">';
			$render .= esc_attr__( 'No blocks found', 'block-analyzer' ) . '</div>';
			$render .= '</div>';
			return $render;
		}

		$render = '<div class="block-analyzer-blocks">';

		foreach ( $blocks as $block_name => $block_count ) {
			ob_start();
			?>
			<div class="block">
				<div class="block__name">
					<?php echo esc_attr( $block_name ); ?>
				</div>
				<div class="block__count"><span>
						<?php echo esc_attr( $block_count ); ?>
					</span></div>
			</div>
			<?php
			$block = ob_get_contents();
			ob_end_clean();
			$render .= $block;
		}

		$render .= '</div>';

		return $render;
	}
}
