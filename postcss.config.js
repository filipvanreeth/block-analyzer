const postcssImport = require('postcss-import');
const postcssSimpleVars = require('postcss-simple-vars');
const postcssNested = require('postcss-nested');
const autoprefixer = require('autoprefixer');
const postcssPresetEnv = require('postcss-preset-env');
const cssnano = require('cssnano');

module.exports = {
	map: { inline: false },
	plugins: [
		postcssImport(),
		postcssSimpleVars(),
		postcssNested(),
		autoprefixer(),
		postcssPresetEnv({
			stage: 1,
			browsers: 'last 2 versions',
			features: {
				'nesting-rules': false,
			},
		}),
		cssnano({
			preset: 'default',
		}),
	],
};
